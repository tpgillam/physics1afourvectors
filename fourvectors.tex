% vim: formatoptions+=t
\documentclass[10pt]{article}

\usepackage{hyperref}
\usepackage{xspace}
\usepackage{units}
\usepackage{hepunits}
\usepackage{heppennames}
\usepackage{hepnicenames}
\usepackage{feynmp}
\DeclareGraphicsRule{*}{mps}{*}{}
\usepackage{subfigure}
\usepackage{relsize}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{snakes}

%% Annotation commands

\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\purple}[1]{{\color{purple} #1}}


%% General purpose commands

\DeclareRobustCommand{\mymath}[1]{\ensuremath{\maybebmsf{#1}}}
\DeclareRobustCommand{\MathUpright}[1]{\mymath{\mathrm{#1}}}
\DeclareRobustCommand{\MathText}[1]{\ensuremath{\text{#1}}}

\newcommand{\bjet}{{\ensuremath{b\textrm{-jet}}}\xspace}
\newcommand{\bjets}{{\ensuremath{b\textrm{-jets}}}\xspace}

%% Commands that can merge subscripts

\makeatletter
\newcommand\newsubcommand[3]{\newcommand#1{#2\sc@sub{#3}}}
\def\sc@sub#1{\def\sc@thesub{#1}\@ifnextchar_{\sc@mergesubs}{_{\sc@thesub}}}
\def\sc@mergesubs_#1{_{\sc@thesub#1}}
\makeatother


%% Common kinematic quantities

\DeclareRobustCommand{\pT}{\mymath{p_\MathUpright{T}}\xspace}
\DeclareRobustCommand{\mT}{\mymath{m_\MathUpright{T}}\xspace}
\DeclareRobustCommand{\Et}{\mymath{E_\MathUpright{T}}\xspace}
\DeclareRobustCommand{\Ht}{\mymath{H_\MathUpright{T}}\xspace}
\DeclareRobustCommand{\meff}{\mymath{m_\MathUpright{eff}}\xspace}
\DeclareRobustCommand{\mll}{\mymath{m_{ll}}\xspace}
\DeclareRobustCommand{\etmiss}{\mymath{E_\MathUpright{T}^\MathUpright{miss}}\xspace}
\newcommand{\met}{\etmiss}
\newcommand{\pt}{\pT}
\newcommand{\mt}{\mT}


\title{Relativistic kinematics for Physics 1A}
% \date{March 2014}
\date{\vspace{-5ex}}
\author{T Gillam}

\begin{document}
\maketitle

\textbf{Notes to reader:} Everywhere I am using $c=1$, so that $\beta=v/c=v$ and
$\gamma=(1-\beta^2)^{-\frac{1}{2}}$. Feel free to re-introduce factors of $c$
for practice! The following also assumes knowledge with some of the results
derived in the course, such as the Lorentz transformations and relativistic
energy and momenta formulae.

\section{What are four vectors?}
In the lectures you have derived the \emph{Lorentz transformation}, which
describes the relationship between events measured in two co-ordinate systems.
We typicaly assume that the frames have their $x$ axes aligned, and that they
are moving relative to each other along this axis; an arbitrary orientation of
each frame can be described by adding in appropriate rotations. The rough
arrangement will be as in Figure \ref{fig:relativeFrames}.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \draw[thick,->] (0,0) -- (3,0) node[anchor=north]{$x$};
    \draw[thick,->] (0,0) -- (0,3) node[anchor=north west]{$S$};

    \draw[semithick,->] (3.4,1.5) -- (4.0,1.5) node[anchor=north]{$\beta$};
    \draw[semithick] (4.0,1.5) -- (4.5,1.5);

    \draw[thick,->] (5,0) -- (8,0) node[anchor=north]{$x'$};
    \draw[thick,->] (5,0) -- (5,3) node[anchor=north west]{$S'$};
  \end{tikzpicture}
  \caption{\label{fig:relativeFrames}Frame $S'$ moving at speed $\beta$ relative to $S$}
\end{figure}

If the co-ordinates of an event in $S'$ is $t',x',y',z'$, then the same event in
$S$ will be seen at $t,x,y,z$. Convince yourself that the Lorentz transformation
relating them can be written in matrix form:
\begin{align}
  \underbrace{\begin{pmatrix}
    t \\ x \\ y \\ z
  \end{pmatrix}}_{\text{\normalsize $X$}}
  &= 
  \underbrace{
  \begin{pmatrix}
    \gamma & \beta\gamma & 0 & 0 \\
    \beta\gamma & \gamma & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1 \\
  \end{pmatrix}}_{\text{\normalsize $\Lambda$}}
  \underbrace{\begin{pmatrix}
    t' \\ x' \\ y' \\ z'
  \end{pmatrix}.}_{\text{\normalsize $X'$}}
\end{align}

Note that we have introduced the four vectors $X$ and $X'$, and the Lorentz
transformation matrix $\Lambda$. We thus have
\begin{align}
  X &= \Lambda X' \\
  \Rightarrow\quad X' &= \Lambda^{-1} X.
  \label{eqn:fourVectorLT}
\end{align} 
By considering the situation with $S$ and $S'$ reversed you should see that, as
a function of $\beta$, $\Lambda^{-1}(\beta) = \Lambda(-\beta)$.

One side note -- the above formulae are written in 3+1D (3 dimensions of
space, and 1 of time), but often for kinematics we will only need 1+1 or
2+1D. I'll be sloppy with notation, so don't be suprised if following
a statement like that in equation \ref{eqn:fourVectorLT} you then see
\begin{align}
  \begin{pmatrix}
    t \\ x \\
  \end{pmatrix}
  &= 
  \gamma\begin{pmatrix}
    1 & \beta \\
    \beta & 1 \\
  \end{pmatrix}
  \begin{pmatrix}
    t' \\ x' \\
  \end{pmatrix}.
  \label{eqn:onePlusOneD}
\end{align}

This concludes the very quick introduction to four vectors! The one thing to
remember is
\begin{quote}
  If a quantity \emph{looks} like a four vector, and \emph{transforms} like one,
  then it \emph{is} a four vector.
\end{quote}
We'll use this later to justify that the four momentum is a four vector.


\subsection{Inner product}
So far we've used four vectors as a way to package co-ordinates, so we can write
the Lorentz transformation in a convenient way. This is already helpful (I think
it easier to remember $\Lambda$ than a set of equations), but the real power
comes with the defintion of an appropriate inner product 
  \footnote{For a good definition,
    see \url{http://mathworld.wolfram.com/InnerProduct.html}.}.

The space of which four vectors are elements is called \emph{Minkowski space},
which is a metric space \footnote{See, for example,
\url{http://mathworld.wolfram.com/MetricSpace.html}},
whose metric we can represent as the matrix
\begin{align}
  \eta &= 
  \begin{pmatrix}
    1 & 0 & 0 & 0 \\
    0 & -1 & 0 & 0 \\
    0 & 0 & -1 & 0 \\
    0 & 0 & 0 & -1 \\
  \end{pmatrix},
\end{align}
so that we can \emph{define} our inner product as
\begin{align}
  A=
  \begin{pmatrix}
    t_A \\ x_A \\ y_A \\ z_A \\
  \end{pmatrix},\quad
  B=
  \begin{pmatrix}
    t_B \\ x_B \\ y_B \\ z_B \\
  \end{pmatrix},\quad
  A\cdot B &= A^\top \eta B.
\end{align}
By considering the matrix multiplication, convince yourself that $A\cdot B = t_A
t_B - x_A x_B - y_A y_B - z_A z_B$. In answering questions, often writing
$A\cdot A$ can become tiresome, so the abbreviation $A^2 = A\cdot A$ will be
applied.

We should now consider what happens when performing this inner product in
another frame. That is, what is $A'\cdot B'$, given that $A = \Lambda A'$, and
similarly for $B$? One finds that
\begin{align}
  A'\cdot B' &= \left( \Lambda^{-1}A \right) \cdot \left( \Lambda^{-1}B \right) \\
  &= \left( \Lambda^{-1}A \right)^\top \eta \left( \Lambda^{-1}B \right) \\
  &= A^\top \underbrace{(\Lambda^{-1})^\top \eta \Lambda^{-1}}_{\eta} B
  \label{eqn:theImportantStep} \\
  &= A\cdot B.
\end{align}
If you can't remember proving the step in equation \ref{eqn:theImportantStep} in
the supervision, do the matrix multiplication to prove this! Dropping down to
1+1D will make it simpler, as in equation \ref{eqn:onePlusOneD}.

This result is even important enough for the calculations we'll be doing to
state again in a different colour.
\purple{
\begin{align}
  A'\cdot B' = A\cdot B
  \label{eqn:innerProductInvariance}
\end{align}
}
The implication is that, when faced with an inner product, we can evaluate it in
\emph{whichever frame is easiest}, and we'll get the same answer. These
quantities are known as \emph{relativistic invariants} \footnote{One can in fact
  derive the Lorentz transformations from a requirement that the metric be
  invariant -- a method favoured by the symmetry-based reasonings that drive
theoretical physics today}.

\subsection{Energy-momentum four vector}
\label{sec:fourMomentum}
You already know that the relativistic energy of the particle is $E=\gamma m$
($c=1$, remember), and the relativistic momentum is $\bm{p} = \gamma m
\bm{v}$. Note that the $\gamma$ here is computed with the \emph{same} $\bm{v}$
that appears in the momentum -- it's describing the amount that the particle's
rest frame is boosted relative to the observer. 
If one defines the quantity
\begin{align}
  P = 
  \begin{pmatrix}
    E \\ \bm{p}
  \end{pmatrix}
  =
  \gamma m\begin{pmatrix}
    1 \\ \bm{v}
  \end{pmatrix},
  \label{eqn:fourMomentumDef}
\end{align}
can you show that it is a four vector? (Consider what you expect this quantity
to be in the rest frame of a given particle, then see what happens when you
boost\footnote{The Lorentz group, of which any given Lorentz transformation is
an element, is that under which the Minkowski metric is invariant. This group
includes all rotations in 3 spatial dimensions (you can prove this!) -- a
`boost' is simply an element of the Lorentz group that does not involve
rotation.}
to an observer's frame).

In the rest frame of the particle you should have convinced yourself that
\begin{align}
  P' = 
  m\begin{pmatrix}
    1 \\ \bm{0}
  \end{pmatrix},
\end{align}
so that $P\cdot P = P'\cdot P' = m^2$.

\subsubsection{Photons}
Photons require some special consideration, since they move at speed 1 and
hence they \emph{do not have a rest frame} -- thus any kind of statement about
their rest mass (e.g. that it is zero) is misleading. Rather, one must consider
basic quantum theory that stipulates that the energy of a photon is related to
its (angular) frequency measured in the observer's frame by $E=\hbar\omega$. Up
to factors of the speed of light, one can then show\footnote{See e.g.
Woodhouse \emph{Special Relativity}, p142-144} that the momentum is the same.
That means one can write 
\newcommand\nhat{\ensuremath{\bm{\hat n}}\xspace}
\newcommand\nhatprime{\ensuremath{\bm{\hat n}'}\xspace}
\begin{align}
  P = \hbar\omega
  \begin{pmatrix}
    1 \\ \nhat \\
  \end{pmatrix},
\end{align}
where \nhat is a unit vector pointing in the direction of propagation of
the photon. Using the usual four vector inner product rules, show that in this
case $P\cdot P = 0$.

\clearpage
\section{Solving kinematics problems}
Four momenta are ideally suited for solving kinematics problems.
\emph{Typically} the workflow is as follows, but in the examples you'll see some
problems where you have to do something a bit different!
\begin{enumerate}
  \item Draw a diagram, label with four vectors \emph{(typically draw in frame in which you want the answer)}
  \item Conserve momentum \& energy
  \item Re-arrange as necessary \emph{(see examples)}
  \item ``Square both sides'' \emph{(take inner product of each side with itsef)}
  \item Manipulate to extract answer
\end{enumerate}

\subsection{Examples}
\subsubsection{Example sheet Q20}
\begin{quote}
A particle of mass $m$ and kinetic energy $K$ strikes and combines with a
stationary particle of mass $2m$, producing a single composite particle of mass
$\sqrt{17}m$. Find the value of $K$.
\end{quote}

First, draw a diagram! I'm labelling with the four vector, then mass.
\begin{center}
  \begin{tikzpicture}
    \draw[thick,->] (1,0) -- (3,0);
    \node[anchor=north] at (2.0,-0.1) {$P,\,m$};
    \fill[black] (4,0) circle (0.1);
    \node[anchor=north] at (4,-0.1) {$Q,\,2m$};

    \draw[semithick,color=gray] (5,0.4) -- (5,-0.6);

    \draw[thick,->] (6.5,0) -- (8,0);
    \node[anchor=north] at (7.25,-0.1) {$R,\,\sqrt{17}m$};
  \end{tikzpicture}
\end{center}

Next conserve energy and momentum. Convince yourself that we can impose both
conditions simultaneously by requiring
\begin{align}
  P+Q=R.
\end{align}
We now want to take advantage of equation \ref{eqn:innerProductInvariance} -- to
do this we take the inner product of each side with itself\footnote{This is
often colloquially referred to as `squaring both sides'. Writing down
abbreviations like $P\cdot P=P^2$ is fine if the meaning is clear from context.}
Doing this we find
\begin{align}
  (P+Q)\cdot(P+Q)&=R\cdot R \\
  P\cdot P + Q\cdot Q + 2 P\cdot Q = R\cdot R \\
  P^2 + Q^2 + 2 P\cdot Q = R^2.
\end{align}
Whenever we see $X^2$, where $X$ is any four momentum, we evaluate the inner
product in the particle's own rest frame. If you work these out, referring to
section \ref{sec:fourMomentum}, you find that for a \emph{massive} particle such
an inner product is always the square of the rest mass. As such, from the
expression above you should find that $P\cdot Q = 6 m^2$.

The question is asking us for the kinetic energy, $K = E_P - m$. Thus we
evaluate the remaining inner product in the lab frame. Since all particles are
moving collinearly, we can work in 1+1 dimensions.
\begin{align}
  P\cdot Q &= 
  \begin{pmatrix}
    E_P \\ p_P\\
  \end{pmatrix}
  \cdot
  \begin{pmatrix}
    2m \\ 0\\
  \end{pmatrix}
  = 2 E_P m  \\
  \Rightarrow\quad E_P &= 3 m \\
  \Rightarrow\quad K &= E_P-m = 2m.
\end{align}
(Factors of $c$ can be reintroduced now -- $K$ is an energy, so $K=2mc^2$)

\subsubsection{Example sheet Q21 (part ii)}
\begin{quote}
A particle of mass $M$ disintegrates while at rest into two parts having masses
of $M/2$ and $M/4$. Show that the relativistic kinetic energies of the parts are
$3Mc^2/32$ and $5Mc^2/32$ respectively.
\end{quote}

\begin{center}
  \begin{tikzpicture}
    \fill[black] (4,0) circle (0.1);
    \node[anchor=north] at (4,-0.1) {$P,\,M$};

    \draw[semithick,color=gray] (5,0.4) -- (5,-0.6);

    \draw[thick,<-] (6.5,0) -- (8,0);
    \node[anchor=north] at (7.25,-0.1) {$Q,\,M/2$};

    \draw[thick,->] (9,0) -- (10.5,0);
    \node[anchor=north] at (9.75,-0.1) {$R,\,M/4$};
  \end{tikzpicture}
\end{center}
We proceed as before; conserving energy and momentum tells us that $P=Q+R$. Now,
one has to be careful about when we take the inner product. Try squaring both
sides as it stands -- you will find an undesirable cross-term in $Q\cdot R$. But
we can avoid this by rearranging to $P-Q=R$, and then solving. Try this, and
show the required answer!

\subsubsection{Example sheet Q23 (part i)}
\newcommand\betaCM{\ensuremath{\beta_{\text{CM}}}\xspace}
\newcommand\gammaCM{\ensuremath{\gamma_{\text{CM}}}\xspace}
\begin{quote}
  A high-energy proton hits a stationary proton and produces a neutral $\pi^0$
  meson (a pion) with mass of 0.144 times the proton mass via the reaction
  \begin{align*}
    p+p\longrightarrow p+p+\pi^0
  \end{align*}
  If the incident proton has just enough energy to allow this reaction to occur,
  what is the velocity (speed $v$ and direction) of the final protons and the
  pion in the laboratory frame of reference?
\end{quote}
\begin{center}
  \begin{tikzpicture}
    \draw[thick,->] (1,0) -- (3,0);
    \node[anchor=north] at (2.0,-0.1) {$P,\,m_p$};
    \fill[black] (4,0) circle (0.1);
    \node[anchor=north] at (4,-0.1) {$Q,\,m_p$};

    \draw[semithick,color=gray] (5,0.7) -- (5,-1.2);

    \draw[thick,->] (6.5,0.5)  node[anchor=east]{$p$}     -- (8,0.5)  node[anchor=west]{$R$};
    \draw[thick,->] (6.5,0)    node[anchor=east]{$p$}     -- (8,0)    node[anchor=west]{$S$};
    \draw[thick,->] (6.5,-0.5) node[anchor=east]{$\pi^0$} -- (8,-0.5) node[anchor=west]{$T$};

    \draw [decorate,decoration={brace,amplitude=5pt}] (8.5,-0.8) -- (6,-0.8);
    \node[anchor=north] at (7.25,-0.9) {$U,\,2m_p+m_{\pi^0}$};
  \end{tikzpicture}
\end{center}
Since we are told that the pion is produced at threshold (``just enough
energy''), we know that in the ZMF of \emph{the system}, all of the final state
particles should be at rest. In the laboratory frame, this means that $R$, $S$
and $T$ correspond to particles with the same velocity \betaCM. As such we can
group all of these together into one four vector $U$ whose mass is the sum of
all three particles. The answer we want, $v=\betaCM$.

A four vector can be written in terms of its mass and velocity (equation
\ref{eqn:fourMomentumDef}), so convince yourself that the answer is most easily
extracted from the product $Q\cdot U$, and that $P\cdot U$ would have an
annoying term. The working should hence look like:
\begin{align}
  P+Q &= U \\
  P   &= U - Q \\
  P^2 &= U^2 + Q^2 - 2 Q\cdot U \\
  (2 m_p + m_{\pi^0})^2 &= 2
  \begin{pmatrix}
    m_p \\ 0 \\
  \end{pmatrix}
  \cdot
  \begin{pmatrix}
    E_U \\ p_U \\
  \end{pmatrix} \\
  &= 2 \gammaCM (2 m_p + m_{\pi^0})
  \begin{pmatrix}
    m_p \\ 0 \\
  \end{pmatrix}
  \cdot
  \begin{pmatrix}
    1 \\ \betaCM \\
  \end{pmatrix} \\
  \gammaCM &= m_p + \frac{m_{\pi^0}}{2},
\end{align}
where $\gammaCM=(1-\betaCM)^{-\frac{1}{2}}$. Solving for the answer is now easy!

\subsubsection{Example sheet Q23 (part ii)}
\begin{quote}
  The neutral pion is observed to decay into two photons of equal energy in the
  laboratory frame. Show that the angle $\theta$ between the $\pi^0$ direction
  and the direction of either of the photons (as observed in the laboratory
  frame) is given by $\cos(\theta)=v/c$, and hence determine the opening angle
  between the two photons.
\end{quote}

In the lab frame:

% Define styles for the different kind of edges in a Feynman diagram
\pgfdeclaredecoration{complete sines}{initial}
{
  \state{initial}[
    width=+0pt,
    next state=sine,
    persistent precomputation={\pgfmathsetmacro\matchinglength{
      \pgfdecoratedinputsegmentlength / int(\pgfdecoratedinputsegmentlength/\pgfdecorationsegmentlength)}
      \setlength{\pgfdecorationsegmentlength}{\matchinglength pt}
  }] {}
  \state{sine}[width=\pgfdecorationsegmentlength]{
    \pgfpathsine{\pgfpoint{0.25\pgfdecorationsegmentlength}{0.5\pgfdecorationsegmentamplitude}}
    \pgfpathcosine{\pgfpoint{0.25\pgfdecorationsegmentlength}{-0.5\pgfdecorationsegmentamplitude}}
    \pgfpathsine{\pgfpoint{0.25\pgfdecorationsegmentlength}{-0.5\pgfdecorationsegmentamplitude}}
    \pgfpathcosine{\pgfpoint{0.25\pgfdecorationsegmentlength}{0.5\pgfdecorationsegmentamplitude}}
  }
  \state{final}{}
}
\tikzset{
    photon/.style={decorate, decoration={complete sines, amplitude=0.2cm,
    segment length=0.3cm}},
    electron/.style={draw=blue, postaction={decorate},
        decoration={markings,mark=at position .55 with {\arrow[draw=blue]{>}}}},
    gluon/.style={decorate, draw=magenta,
        decoration={coil,amplitude=4pt, segment length=5pt}} 
}
\newcommand\xuvec{\ensuremath{\bm{\hat{x}}}\xspace}
\newcommand\yuvec{\ensuremath{\bm{\hat{y}}}\xspace}
\begin{center}
  \begin{tikzpicture}
    \draw[->] (1,0) -- (1.8,0) node[anchor=north]{\xuvec};
    \draw[->] (1,0) -- (1,0.8) node[anchor=north east]{\yuvec};

    \draw[thick,->] (3,0) -- (5,0);
    \node[anchor=north] at (4.0,-0.1) {$P,\,m_{\pi^0}$};

    \draw[semithick,color=gray] (5.5,0.4) -- (5.5,-0.6);

    \draw[dashed] (6,0) -- (10,0);
    \draw[photon] (6.5,0) -- (9,1.3) node[anchor=west] {$Q$};
    \draw[photon] (6.5,0) -- (9,-1.3) node[anchor=west] {$R$};
    \draw (6.5,0) ++(1.5,0) arc (0:atan(1.3/2.5):1.5);
    \draw (6.5,0) ++(1.55,0) arc (0:atan(1.3/2.5):1.55);
    \draw (6.5,0) ++(1.6,0) arc (0:-atan(1.3/2.5):1.6);
    \draw (6.5,0) ++(1.65,0) arc (0:-atan(1.3/2.5):1.65);
    \node[anchor=west] at (8.0,0.4) {$\theta$};
  \end{tikzpicture}
\end{center}

It should be clear that for this problem we will need to work in 2+1 dimensions.
The spatial unit vectors \xuvec and \yuvec are defined in the diagram. Since we
are told that the photons have the same energy, and they must also have the same
energy in the rest frame (why?), each will have the same angle $\theta$ wrt. the
$\pi^0$. 

In the lab frame, the four vectors we have labelled are going to look like
\begin{align}
  P=\begin{pmatrix}
    E_{\pi^0} \\ p_{\pi^0} \xuvec \\
  \end{pmatrix} =
  \gamma m_{\pi^0}\begin{pmatrix}
    1 \\ \beta \xuvec \\
  \end{pmatrix}
  ,\qquad
  Q=E_\gamma\begin{pmatrix}
    1 \\ \nhat \\
  \end{pmatrix}
  ,\qquad
  R=E_\gamma\begin{pmatrix}
    1 \\ \nhatprime \\
  \end{pmatrix},
\end{align}
with $\nhat=\cos\theta\xuvec + \sin\theta\yuvec$ and
$\nhatprime=\cos\theta\xuvec - \sin\theta\yuvec$, and $\gamma$ and $\beta$
referring to the motion of the $\pi^0$ (i.e. the ZMF of this problem). In this
case the `squaring both sides' trick won't work -- whatever you try will give
you a mess of undesirable cross terms (try it out!). But, we \emph{can} compare
spatial and temporal components separately
\begin{align}
  P &= Q + R \\
  \Rightarrow\quad
    \gamma m_{\pi^0} &= 2 E_\gamma  \\
  \text{and}\quad \gamma m_{\pi^0} \beta &= 2 E_\gamma \cos\theta \\
  \Rightarrow\quad \cos\theta &= \beta.
\end{align}


This problem can also be solved fairly easily using velocity transformation laws
-- have a go!

\subsubsection{Compton scattering}
Adapted from Woodhouse, \emph{Special Relativity}, p145.
\begin{quote}
  A photon of [angular] frequency $\omega$ collides with an electron of rest
  mass $m$, which is initially at rest. After the collision, the photon has
  frequency $\omega'$. Show that
  \begin{align*}
    \hbar\omega\omega'(1-cos\theta) = mc^2(\omega-\omega'),
  \end{align*}
  where $\theta$ is the angle between the initial and final trajectories of the
  photon.
\end{quote}

\begin{center}
  \begin{tikzpicture}
    \draw[->] (0.5,0) -- (1.3,0) node[anchor=north]{\xuvec};
    \draw[->] (0.5,0) -- (0.5,0.8) node[anchor=north east]{\yuvec};

    \draw[photon] (2,0) -- (4,0);
    \node[anchor=north] at (3,-0.1) {$P,\,\omega$};
    \fill[black] (4.8,0) circle (0.1);
    \node[anchor=north] at (4.8,-0.1) {$Q,\,m$};

    \draw[semithick,color=gray] (5.5,0.4) -- (5.5,-0.6);

    \draw[dashed] (6,0) -- (10,0);
    \draw[photon] (6.5,0) -- (9,1.3) node[anchor=west] {$R,\,\omega'$};
    \draw[thick,->] (6.5,0) -- (8,-1.0) node[anchor=west] {$S,\,m$};
    \draw (6.5,0) ++(1.5,0) arc (0:atan(1.3/2.5):1.5);
    \node[anchor=west] at (8.0,0.4) {$\theta$};
  \end{tikzpicture}
\end{center}
Conserving momentum and energy as usual gives us $P+Q=R+S$. What inner product
do we particularly need (i.e. which is in terms of the quantities asked for in
the question)? Which inner products would introduce quantities we don't want to
think about? From such considerations, conclude that one must start from
\begin{align}
  P+Q-R=S,
\end{align}
before squaring both sides. Perform the calculation using the same techniques as
previously, noting that one may write
\begin{align}
  P=\hbar\omega\begin{pmatrix}
    1 \\ \xuvec \\
  \end{pmatrix},\qquad
  R=\hbar\omega'\begin{pmatrix}
    1 \\ \nhat \\
  \end{pmatrix}
  ,
\end{align}
where \nhat is a unit vector pointing in the direction of propagation of
the resultant photon.


\end{document}
