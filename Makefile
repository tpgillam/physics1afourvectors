fourvectors:
	pdflatex fourvectors
	pdflatex fourvectors

clean:
	find . -type f | grep '~'$ | xargs rm
	rm -f *.log
	rm -f *.aux
	rm -f *.out
	rm -f *.toc
	rm -f comment.cut
	rm -f *.bbl
	rm -f *.blg
	rm -f *.lof
	rm -f *.lot
